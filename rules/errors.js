module.exports = {
	rules: {
		// https://eslint.org/docs/rules/no-console
		'no-console': [
			2,
			{
				allow: ['warn', 'error'],
			},
		],

		// https://eslint.org/docs/rules/require-await
		"require-await": "error",
	},
};
