module.exports = {
	rules: {
		// eslint-plugin-react
		'react/react-in-jsx-scope': 0,
		'react/display-name': 0,
		'react/prop-types': 0,
		'react/self-closing-comp': [
			'error',
			{
				component: true,
				html: true,
			},
		],
		'react/destructuring-assignment': 'error',
		'react/jsx-boolean-value': 'off',
		'react/jsx-key': 'error',
		'react/jsx-sort-props': [
			2,
			{
				noSortAlphabetically: true,
				reservedFirst: true,
			},
		],
		'react/jsx-wrap-multilines': 'error',
		// eslint-plugin-react-hooks
		'react-hooks/rules-of-hooks': 'error',
		'react-hooks/exhaustive-deps': 'error',
	},
};
