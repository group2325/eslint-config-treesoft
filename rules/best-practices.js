module.exports = {
	rules: {
		// require the use of === and !==
		// http://eslint.org/docs/rules/eqeqeq
		eqeqeq: ['error', 'always'],

		// disallow implicit type conversions
		// http://eslint.org/docs/rules/no-implicit-coercion
		'no-implicit-coercion': [
			'error',
			{
				boolean: true,
				number: true,
				string: true,
			},
		],

		// disallow reassignment of function parameters
		// disallow parameter object manipulation
		// rule: http://eslint.org/docs/rules/no-param-reassign.html
		'no-param-reassign': [
			'error',
			{
				props: true,
				ignorePropertyModificationsFor: ['state'],
			},
		],

		// restrict what can be thrown as an exception
		'no-throw-literal': 'error',
	},
};
