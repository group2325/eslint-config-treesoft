module.exports = {
	rules: {
		'@typescript-eslint/explicit-function-return-type': 0,
		'@typescript-eslint/explicit-member-accessibility': 0,
		'@typescript-eslint/indent': 0,
		'@typescript-eslint/member-delimiter-style': 0,
		'@typescript-eslint/no-explicit-any': 0,
		'@typescript-eslint/no-var-requires': 0,
		'@typescript-eslint/no-use-before-define': 0,
		'@typescript-eslint/no-unused-vars': [
			2,
			{
				argsIgnorePattern: '^_',
			},
		],
		'@typescript-eslint/naming-convention': [
			'error',
			{
				selector: ['variable'],
				format: ['camelCase', 'PascalCase', 'UPPER_CASE'],
			},
			{
				selector: ['enum', 'enumMember', 'interface'],
				format: ['PascalCase'],
			},
		],
		'@typescript-eslint/no-shadow': 'error',
	},
};
