module.exports = {
	extends: [
		'./rules/best-practices.js',
		'./rules/errors.js',
		'./rules/imports.js',
		'./rules/react.js',
		'./rules/style.js',
		'./rules/tests.js',
		'./rules/typescript.js',
	],
	parserOptions: {
		ecmaVersion: 2018,
	},
	globals: {
		$: 'writeable',
	},
	env: {
		browser: true,
		node: true,
	},
};